const COS = require('cos-js-sdk-v5')
import { uploadVal } from '../env'
import { FUNS } from '../api/getRequestFuns'

const cos = new COS({
  // 必选参数
  getAuthorization: async function(options: any, callback: Function) {
    // 服务端 JS 和 PHP 例子：https://github.com/tencentyun/cos-js-sdk-v5/blob/master/server/
    // 服务端其他语言参考 COS STS SDK ：https://github.com/tencentyun/qcloud-cos-sts-sdk
    // STS 详细文档指引看：https://cloud.tencent.com/document/product/436/14048
    const res: _credential._rgenerateKey = await FUNS._credentialgenerateKey({ data: {} })
    if (res.code !== '0000') return false
    const obj = {
      TmpSecretId: res.data.credentialsCliVO.tmpSecretId,
      TmpSecretKey: res.data.credentialsCliVO.tmpSecretKey,
      XCosSecurityToken: res.data.credentialsCliVO.sessionToken,
      // 建议返回服务器时间作为签名的开始时间，避免用户浏览器本地时间偏差过大导致签名错误
      // 时间戳，单位秒，如：1580000000
      StartTime: res.data.startTime,
      // 时间戳，单位秒，如：1580000900
      ExpiredTime: res.data.expiredTime
    }
    callback(obj)
  }
})

/**
 * 图片上传
 * @param file 图片
 * @param path 路径前缀
 */
export const upload = function(file: any, path?: string): Promise<string> {
  return new Promise((resolve) => {
    const date = new Date()
    const math = parseInt(`${Math.random() * 100000}`)
    const name = `${date.getTime()}${math}`
    cos.putObject({
      Bucket: uploadVal.Bucket /* 必须 */,
      Region: uploadVal.Region /* 存储桶所在地域，必须字段 */,
      Key: `${path || '/jsb/prize'}/${date.getFullYear()}/${date.getMonth() + 1}/${date.getDate()}/${name}.jpg` /* 必须 */,
      Body: file /* 上传文件对象 */
    }, (err: any, data: any) => {
      // 图片上传失败
      if (err) {
        return resolve('')
      }
      resolve(`https://${data.Location}`)
    })
  })
}
