import store from 'store'

// App
const sidebarStatusKey = 'sidebar_status'
export const getSidebarStatus = () => store.get(sidebarStatusKey)
export const setSidebarStatus = (sidebarStatus: string) => store.set(sidebarStatusKey, sidebarStatus)

const languageKey = 'language'
export const getLanguage = () => store.get(languageKey)
export const setLanguage = (language: string) => store.set(languageKey, language)

const sizeKey = 'size'
export const getSize = () => store.get(sizeKey)
export const setSize = (size: string) => store.set(sizeKey, size)

// User
const tokenKey = 'jl-top-admin-website'
export const getToken = () => store.get(tokenKey)
export const setToken = (token: string) => store.set(tokenKey, token)
export const removeToken = () => store.set(tokenKey, '')

// status
const statusKey = 'lstatus'
export const getStatus = () => store.get(statusKey)
export const setStatus = (Status: string | number) => store.set(statusKey, Status.toString())
export const removeStatus = () => store.set(statusKey, '')
