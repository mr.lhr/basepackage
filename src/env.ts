/*
 * 配置编译环境和线上环境之间的切换
 */
const NODE_ENV: any = {
  development: 'http://192.168.3.220:8202',
  production: 'http://jsbapi.zcyuan.cn',
  testing: 'http://192.168.3.220:8202',
  devTest: 'http://192.168.3.220:8202',
  default: 'http://192.168.3.220:8202'
}

const base: any = NODE_ENV[process.env.NODE_ENV || 'default']

/**
 * 图片上传桶
 */
interface wxupload {
  Bucket: string
  Region: string
}

/**
 * 仓库 key值
 */
const storeKey = 'admin'

const uploadVal = {
  Bucket: 'wjh-t-1300238208',
  Region: 'ap-guangzhou'
} as wxupload

if (process.env.NODE_ENV === 'production') {
  uploadVal.Bucket = 'wjh-1300238208'
}

export {
  base,
  uploadVal,
  storeKey
}
