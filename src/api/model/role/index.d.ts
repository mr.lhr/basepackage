/**
 * 角色接口
 * create: lihuarong<517849815@qq.com>
 * time: 2020-06-10 16:50:57
 */
declare namespace _role {
  interface SysRoleDO {
    /**
     * 
     */
    companyName: string,
    /**
     * 
     */
    createdBy: string,
    /**
     * 
     */
    createdDate: string,
    /**
     * 
     */
    delFlag: boolean,
    /**
     * 
     */
    department: string,
    /**
     * 
     */
    isDefault: boolean,
    /**
     * 
     */
    lastModifiedBy: string,
    /**
     * 
     */
    lastModifiedDate: string,
    /**
     * 
     */
    orgId: string,
    /**
     * 
     */
    parentId: number,
    /**
     * 
     */
    position: string,
    /**
     * 
     */
    rights: string,
    /**
     * 
     */
    roleId: number,
    /**
     * 
     */
    roleName: string
  }
  interface ResultBeanVoid extends _default.res {
  }
  interface AuthcEditRequest extends _default.post {
    /**
     * 菜单ID
     */
    menuIds: string,
    /**
     * 角色ID
     */
    roleId: string
  }
  interface RoleRequest extends _default.post {
    /**
     * 角色ID
     */
    roleId: string
  }
  interface ResultBeanAuthcMenuVO extends _default.res {
    /**
     * 
     */
    data: _role.AuthcMenuVO
  }
  interface AuthcMenuVO {
    /**
     * 
     */
    idList: string[],
    /**
     * 
     */
    menuList: _role.Menu[]
  }
  interface Menu {
    /**
     * 
     */
    hasMenu: boolean,
    /**
     * 
     */
    isParent: boolean,
    /**
     * 
     */
    menuIcon: string,
    /**
     * 
     */
    menuId: number,
    /**
     * 
     */
    menuName: string,
    /**
     * 
     */
    menuOrder: string,
    /**
     * 
     */
    menuState: string,
    /**
     * 
     */
    menuType: string,
    /**
     * 
     */
    menuUrl: string,
    /**
     * 
     */
    parentId: string,
    /**
     * 
     */
    parentMenu: _role.Menu,
    /**
     * 
     */
    subMenu: _role.Menu[],
    /**
     * 
     */
    target: string
  }
  interface ResultBeanSysRoleVO extends _default.res {
    /**
     * 
     */
    data: _role.SysRoleVO
  }
  interface SysRoleVO {
    /**
     * 
     */
    companyName: string,
    /**
     * 
     */
    createdBy: string,
    /**
     * 
     */
    createdDate: string,
    /**
     * 
     */
    default: boolean,
    /**
     * 
     */
    delFlag: boolean,
    /**
     * 
     */
    department: string,
    /**
     * 
     */
    lastModifiedBy: string,
    /**
     * 
     */
    lastModifiedDate: string,
    /**
     * 
     */
    orgId: string,
    /**
     * 
     */
    parentId: string,
    /**
     * 
     */
    position: string,
    /**
     * 
     */
    rights: string,
    /**
     * 
     */
    roleId: string,
    /**
     * 
     */
    roleName: string
  }
  interface RoleArrayRequest extends _default.post {
    /**
     * 组织ID
     */
    orgId: string
  }
  interface ResultBeanListSysRoleVO extends _default.res {
    /**
     * 
     */
    data: _role.SysRoleVO[]
  }
  interface RoleListRequest extends _default.post {
    /**
     * 组织ID
     */
    orgId: string,
    /**
     * 角色组ID
     */
    roleId: string
  }
  interface ResultBeanListUserRolesVO extends _default.res {
    /**
     * 
     */
    data: _role.UserRolesVO[]
  }
  interface UserRolesVO {
    /**
     * 
     */
    childRoleList: _role.SysRoleVO[],
    /**
     * 
     */
    roleId: string,
    /**
     * 
     */
    roleName: string
  }
  interface OrgRolesRequest extends _default.post {
    /**
     * 组织ID
     */
    orgId: string
  }
  /**
   * 新增角色 post
   */
  interface _padd extends _role.SysRoleDO {
  }
  /**
   * 新增角色 res
   */
  interface _radd extends _role.ResultBeanVoid {
  }
  /**
   * 编辑菜单权限 post
   */
  interface _pauthcEdit extends _role.AuthcEditRequest {
  }
  /**
   * 编辑菜单权限 res
   */
  interface _rauthcEdit extends _role.ResultBeanVoid {
  }
  /**
   * 菜单授权菜单 post
   */
  interface _pauthcMenu extends _role.RoleRequest {
  }
  /**
   * 菜单授权菜单 res
   */
  interface _rauthcMenu extends _role.ResultBeanAuthcMenuVO {
  }
  /**
   * 删除角色 post
   */
  interface _pdelete extends _role.RoleRequest {
  }
  /**
   * 删除角色 res
   */
  interface _rdelete extends _role.ResultBeanVoid {
  }
  /**
   * 角色详情 post
   */
  interface _pdetail extends _role.RoleRequest {
  }
  /**
   * 角色详情 res
   */
  interface _rdetail extends _role.ResultBeanSysRoleVO {
  }
  /**
   * 编辑角色 post
   */
  interface _pedit extends _role.SysRoleDO {
  }
  /**
   * 编辑角色 res
   */
  interface _redit extends _role.ResultBeanVoid {
  }
  /**
   * 角色组列表 post
   */
  interface _pgroupList extends _role.RoleArrayRequest {
  }
  /**
   * 角色组列表 res
   */
  interface _rgroupList extends _role.ResultBeanListSysRoleVO {
  }
  /**
   * 角色列表 post
   */
  interface _plist extends _role.RoleListRequest {
  }
  /**
   * 角色列表 res
   */
  interface _rlist extends _role.ResultBeanListSysRoleVO {
  }
  /**
   * 用户角色列表 post
   */
  interface _plistAllRoles {
  }
  /**
   * 用户角色列表 res
   */
  interface _rlistAllRoles extends _role.ResultBeanListUserRolesVO {
  }
  /**
   * 组织角色列表 post
   */
  interface _porgRoles extends _role.OrgRolesRequest {
  }
  /**
   * 组织角色列表 res
   */
  interface _rorgRoles extends _role.ResultBeanListUserRolesVO {
  }
}
declare module _default {
  interface apifuns {
    /**
     * 新增角色
     */
    _roleadd({ data, taht }: { data: _role._padd, taht?: any }): _role._radd,
    /**
     * 编辑菜单权限
     */
    _roleauthcEdit({ data, taht }: { data: _role._pauthcEdit, taht?: any }): _role._rauthcEdit,
    /**
     * 菜单授权菜单
     */
    _roleauthcMenu({ data, taht }: { data: _role._pauthcMenu, taht?: any }): _role._rauthcMenu,
    /**
     * 删除角色
     */
    _roledelete({ data, taht }: { data: _role._pdelete, taht?: any }): _role._rdelete,
    /**
     * 角色详情
     */
    _roledetail({ data, taht }: { data: _role._pdetail, taht?: any }): _role._rdetail,
    /**
     * 编辑角色
     */
    _roleedit({ data, taht }: { data: _role._pedit, taht?: any }): _role._redit,
    /**
     * 角色组列表
     */
    _rolegroupList({ data, taht }: { data: _role._pgroupList, taht?: any }): _role._rgroupList,
    /**
     * 角色列表
     */
    _rolelist({ data, taht }: { data: _role._plist, taht?: any }): _role._rlist,
    /**
     * 用户角色列表
     */
    _rolelistAllRoles({ data, taht }: { data: _role._plistAllRoles, taht?: any }): _role._rlistAllRoles,
    /**
     * 组织角色列表
     */
    _roleorgRoles({ data, taht }: { data: _role._porgRoles, taht?: any }): _role._rorgRoles
  }
}
