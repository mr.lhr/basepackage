export default {
  /**
   * post /role/add 新增角色
   */
  _roleadd: { method: 'post', url: '/role/add' },
  /**
   * post /role/authcEdit 编辑菜单权限
   */
  _roleauthcEdit: { method: 'post', url: '/role/authcEdit' },
  /**
   * post /role/authcMenu 菜单授权菜单
   */
  _roleauthcMenu: { method: 'post', url: '/role/authcMenu' },
  /**
   * post /role/delete 删除角色
   */
  _roledelete: { method: 'post', url: '/role/delete' },
  /**
   * post /role/detail 角色详情
   */
  _roledetail: { method: 'post', url: '/role/detail' },
  /**
   * post /role/edit 编辑角色
   */
  _roleedit: { method: 'post', url: '/role/edit' },
  /**
   * post /role/groupList 角色组列表
   */
  _rolegroupList: { method: 'post', url: '/role/groupList' },
  /**
   * post /role/list 角色列表
   */
  _rolelist: { method: 'post', url: '/role/list' },
  /**
   * get /role/listAllRoles 用户角色列表
   */
  _rolelistAllRoles: { method: 'get', url: '/role/listAllRoles' },
  /**
   * post /role/orgRoles 组织角色列表
   */
  _roleorgRoles: { method: 'post', url: '/role/orgRoles' }
}
