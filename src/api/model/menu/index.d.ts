/**
 * 菜单接口
 * create: lihuarong<517849815@qq.com>
 * time: 2020-06-10 16:50:58
 */
declare namespace _menu {
  interface AddMenuRequest extends _default.post {
    /**
     * 菜单名称
     */
    menuName: string,
    /**
     * 序号
     */
    menuOrder: string,
    /**
     * 菜单状态
     */
    menuState: number,
    /**
     * 菜单类型
     */
    menuType: string,
    /**
     * 菜单地址
     */
    menuUrl: string,
    /**
     * 父ID
     */
    parentId: string
  }
  interface ResultBeanVoid extends _default.res {
  }
  interface SysMenuDO {
    /**
     * 
     */
    createdBy: string,
    /**
     * 
     */
    createdDate: string,
    /**
     * 
     */
    delFlag: boolean,
    /**
     * 
     */
    lastModifiedBy: string,
    /**
     * 
     */
    lastModifiedDate: string,
    /**
     * 菜单图标
     */
    menuIcon: string,
    /**
     * 
     */
    menuId: number,
    /**
     * 菜单名
     */
    menuName: string,
    /**
     * 序号
     */
    menuOrder: string,
    /**
     * 菜单状态
     */
    menuState: number,
    /**
     * 菜单类型
     */
    menuType: string,
    /**
     * 菜单地址
     */
    menuUrl: string,
    /**
     * 父级ID
     */
    parentId: string
  }
  interface ResultBeanSysMenuVO extends _default.res {
    /**
     * 
     */
    data: _menu.SysMenuVO
  }
  interface SysMenuVO {
    /**
     * 
     */
    createdBy: string,
    /**
     * 
     */
    createdDate: string,
    /**
     * 
     */
    delFlag: boolean,
    /**
     * 
     */
    lastModifiedBy: string,
    /**
     * 
     */
    lastModifiedDate: string,
    /**
     * 
     */
    menuIcon: string,
    /**
     * 
     */
    menuId: number,
    /**
     * 
     */
    menuName: string,
    /**
     * 
     */
    menuOrder: string,
    /**
     * 
     */
    menuState: string,
    /**
     * 
     */
    menuType: string,
    /**
     * 
     */
    menuUrl: string,
    /**
     * 
     */
    parentId: string,
    /**
     * 
     */
    parentName: string
  }
  interface ResultBeanListMenu extends _default.res {
    /**
     * 
     */
    data: _menu.Menu[]
  }
  interface Menu {
    /**
     * 
     */
    hasMenu: boolean,
    /**
     * 
     */
    isParent: boolean,
    /**
     * 
     */
    menuIcon: string,
    /**
     * 
     */
    menuId: number,
    /**
     * 
     */
    menuName: string,
    /**
     * 
     */
    menuOrder: string,
    /**
     * 
     */
    menuState: string,
    /**
     * 
     */
    menuType: string,
    /**
     * 
     */
    menuUrl: string,
    /**
     * 
     */
    parentId: string,
    /**
     * 
     */
    parentMenu: _menu.Menu,
    /**
     * 
     */
    subMenu: _menu.Menu[],
    /**
     * 
     */
    target: string
  }
  interface ResultBeanListSysMenuVO extends _default.res {
    /**
     * 
     */
    data: _menu.SysMenuVO[]
  }
  interface ResultBeanMenuVO extends _default.res {
    /**
     * 
     */
    data: _menu.MenuVO
  }
  interface MenuVO {
    /**
     * 
     */
    backId: string,
    /**
     * 
     */
    menus: _menu.SysMenuVO[]
  }
  /**
   * 添加菜单 post
   */
  interface _padd extends _menu.AddMenuRequest {
  }
  /**
   * 添加菜单 res
   */
  interface _radd extends _menu.ResultBeanVoid {
  }
  /**
   * 删除菜单 post
   */
  interface _pdelete extends _menu.SysMenuDO {
  }
  /**
   * 删除菜单 res
   */
  interface _rdelete extends _menu.ResultBeanVoid {
  }
  /**
   * 菜单详情 post
   */
  interface _pdetail extends _menu.SysMenuDO {
  }
  /**
   * 菜单详情 res
   */
  interface _rdetail extends _menu.ResultBeanSysMenuVO {
  }
  /**
   * 修改菜单 post
   */
  interface _pedit extends _menu.SysMenuDO {
  }
  /**
   * 修改菜单 res
   */
  interface _redit extends _menu.ResultBeanVoid {
  }
  /**
   * 首页菜单列表 post
   */
  interface _pindexMenuList {
  }
  /**
   * 首页菜单列表 res
   */
  interface _rindexMenuList extends _menu.ResultBeanListMenu {
  }
  /**
   * 显示菜单列表 post
   */
  interface _plist extends _menu.SysMenuDO {
  }
  /**
   * 显示菜单列表 res
   */
  interface _rlist extends _menu.ResultBeanListSysMenuVO {
  }
  /**
   * 菜单树列表 post
   */
  interface _plistAllMenu {
  }
  /**
   * 菜单树列表 res
   */
  interface _rlistAllMenu extends _menu.ResultBeanListMenu {
  }
  /**
   * 显示菜单列表 post
   */
  interface _pmenus extends _menu.SysMenuDO {
  }
  /**
   * 显示菜单列表 res
   */
  interface _rmenus extends _menu.ResultBeanMenuVO {
  }
}
declare module _default {
  interface apifuns {
    /**
     * 添加菜单
     */
    _menuadd({ data, taht }: { data: _menu._padd, taht?: any }): _menu._radd,
    /**
     * 删除菜单
     */
    _menudelete({ data, taht }: { data: _menu._pdelete, taht?: any }): _menu._rdelete,
    /**
     * 菜单详情
     */
    _menudetail({ data, taht }: { data: _menu._pdetail, taht?: any }): _menu._rdetail,
    /**
     * 修改菜单
     */
    _menuedit({ data, taht }: { data: _menu._pedit, taht?: any }): _menu._redit,
    /**
     * 首页菜单列表
     */
    _menuindexMenuList({ data, taht }: { data: _menu._pindexMenuList, taht?: any }): _menu._rindexMenuList,
    /**
     * 显示菜单列表
     */
    _menulist({ data, taht }: { data: _menu._plist, taht?: any }): _menu._rlist,
    /**
     * 菜单树列表
     */
    _menulistAllMenu({ data, taht }: { data: _menu._plistAllMenu, taht?: any }): _menu._rlistAllMenu,
    /**
     * 显示菜单列表
     */
    _menumenus({ data, taht }: { data: _menu._pmenus, taht?: any }): _menu._rmenus
  }
}
