export default {
  /**
   * post /menu/add 添加菜单
   */
  _menuadd: { method: 'post', url: '/menu/add' },
  /**
   * post /menu/delete 删除菜单
   */
  _menudelete: { method: 'post', url: '/menu/delete' },
  /**
   * post /menu/detail 菜单详情
   */
  _menudetail: { method: 'post', url: '/menu/detail' },
  /**
   * post /menu/edit 修改菜单
   */
  _menuedit: { method: 'post', url: '/menu/edit' },
  /**
   * get /menu/indexMenuList 首页菜单列表
   */
  _menuindexMenuList: { method: 'get', url: '/menu/indexMenuList' },
  /**
   * post /menu/list 显示菜单列表
   */
  _menulist: { method: 'post', url: '/menu/list' },
  /**
   * get /menu/listAllMenu 菜单树列表
   */
  _menulistAllMenu: { method: 'get', url: '/menu/listAllMenu' },
  /**
   * post /menu/menus 显示菜单列表
   */
  _menumenus: { method: 'post', url: '/menu/menus' }
}
