export default {
  /**
   * post /topAdmin/credential/generateKey 临时密钥生成
   */
  _credentialgenerateKey: { method: 'post', url: '/topAdmin/credential/generateKey' }
}
