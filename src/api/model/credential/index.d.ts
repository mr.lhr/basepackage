/**
 * 密钥
 * create: lihuarong<517849815@qq.com>
 * time: 2020-06-10 16:50:57
 */
declare namespace _credential {
  interface ResultBeanCredentialsInfoCliVO extends _default.res {
    /**
     * 
     */
    data: _credential.CredentialsInfoCliVO
  }
  interface CredentialsInfoCliVO {
    /**
     * 区域
     */
    apRegion: string,
    /**
     * 存储桶名称
     */
    bucketName: string,
    /**
     * 临时密钥信息
     */
    credentialsCliVO: _credential.CredentialsCliVO,
    /**
     * 临时密钥 Key
     */
    expiredTime: string,
    /**
     * 临时密钥 Id
     */
    startTime: string
  }
  interface CredentialsCliVO {
    /**
     * 请求时需要用的 token 字符串
     */
    sessionToken: string,
    /**
     * 临时密钥 Id
     */
    tmpSecretId: string,
    /**
     * 临时密钥 Key
     */
    tmpSecretKey: string
  }
  /**
   * 临时密钥生成 post
   */
  interface _pgenerateKey {
  }
  /**
   * 临时密钥生成 res
   */
  interface _rgenerateKey extends _credential.ResultBeanCredentialsInfoCliVO {
  }
}
declare module _default {
  interface apifuns {
    /**
     * 临时密钥生成
     */
    _credentialgenerateKey({ data, taht }: { data: _credential._pgenerateKey, taht?: any }): _credential._rgenerateKey
  }
}
