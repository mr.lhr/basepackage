/**
 * 用户接口
 * create: lihuarong<517849815@qq.com>
 * time: 2020-06-10 16:50:57
 */
declare namespace _user {
  interface DeleteUserRequest extends _default.post {
    /**
     * 用户ID
     */
    userId: string
  }
  interface ResultBeanVoid extends _default.res {
  }
  interface EditPasswordRequest extends _default.post {
    /**
     * 新密码
     */
    newPassword: string,
    /**
     * 原密码
     */
    oldPassword: string,
    /**
     * 用户名
     */
    userName: string
  }
  interface ResultBeanstring extends _default.res {
    /**
     * 
     */
    data: string
  }
  interface EditUserRequest extends _default.post {
    /**
     * 备注
     */
    bz: string,
    /**
     * 邮箱
     */
    email: string,
    /**
     * 姓名
     */
    name: string,
    /**
     * 编号
     */
    number: string,
    /**
     * 密码
     */
    password: string,
    /**
     * 手机号
     */
    phone: string,
    /**
     * 角色ID
     */
    roleId: number,
    /**
     * 用户ID
     */
    userId: string,
    /**
     * 用户名
     */
    username: string
  }
  interface UsersRequest extends _default.post {
    /**
     * 页码
     */
    currentPage: number,
    /**
     * 关键词
     */
    keywords: string,
    /**
     * 结束日期
     */
    lastLoginEnd: string,
    /**
     * 开始日期
     */
    lastLoginStart: string,
    /**
     * 组织ID
     */
    orgId: string,
    /**
     * 页面大小
     */
    showCount: number
  }
  interface ResultBeanPageDataListUserVO extends _default.res {
    /**
     * 
     */
    data: _user.PageDataListUserVO
  }
  interface PageDataListUserVO {
    /**
     * 数据集合
     */
    rows: _user.ListUserVO[],
    /**
     * 总行数
     */
    total: number
  }
  interface ListUserVO {
    /**
     * 
     */
    email: string,
    /**
     * 
     */
    ip: string,
    /**
     * 
     */
    lastLogin: string,
    /**
     * 
     */
    name: string,
    /**
     * 
     */
    number: string,
    /**
     * 
     */
    orgId: string,
    /**
     * 
     */
    orgName: string,
    /**
     * 
     */
    password: string,
    /**
     * 
     */
    phone: string,
    /**
     * 
     */
    roleId: string,
    /**
     * 
     */
    roleName: string,
    /**
     * 
     */
    userId: string,
    /**
     * 
     */
    username: string
  }
  interface ListUsersRequest extends _default.post {
    /**
     * 页码
     */
    currentPage: number,
    /**
     * 关键词
     */
    keywords: string,
    /**
     * 结束日期
     */
    lastLoginEnd: string,
    /**
     * 开始日期
     */
    lastLoginStart: string,
    /**
     * 角色ID
     */
    roleId: string,
    /**
     * 页面大小
     */
    showCount: number
  }
  interface ResultBeanSysUserVO extends _default.res {
    /**
     * 
     */
    data: _user.SysUserVO
  }
  interface SysUserVO {
    /**
     * 
     */
    bz: string,
    /**
     * 
     */
    companyName: string,
    /**
     * 
     */
    createdBy: string,
    /**
     * 
     */
    createdDate: string,
    /**
     * 
     */
    delFlag: boolean,
    /**
     * 
     */
    department: string,
    /**
     * 
     */
    email: string,
    /**
     * 
     */
    head: string,
    /**
     * 
     */
    ip: string,
    /**
     * 
     */
    lastLogin: string,
    /**
     * 
     */
    lastModifiedBy: string,
    /**
     * 
     */
    lastModifiedDate: string,
    /**
     * 
     */
    name: string,
    /**
     * 
     */
    number: string,
    /**
     * 
     */
    orgId: string,
    /**
     * 
     */
    parentRoleId: string,
    /**
     * 
     */
    phone: string,
    /**
     * 
     */
    position: string,
    /**
     * 
     */
    roleId: string,
    /**
     * 
     */
    skin: string,
    /**
     * 
     */
    status: string,
    /**
     * 
     */
    type: string,
    /**
     * 
     */
    userId: string,
    /**
     * 
     */
    username: string
  }
  interface SaveUserRequest extends _default.post {
    /**
     * 备注
     */
    bz: string,
    /**
     * 邮箱
     */
    email: string,
    /**
     * 姓名
     */
    name: string,
    /**
     * 编号
     */
    number: string,
    /**
     * 密码
     */
    password: string,
    /**
     * 手机号
     */
    phone: string,
    /**
     * 角色ID
     */
    roleId: number,
    /**
     * 用户名
     */
    username: string
  }
  interface UserDetailRequest extends _default.post {
    /**
     * 用户ID
     */
    userId: string
  }
  /**
   * 删除系统用户 post
   */
  interface _pdeleteUser extends _user.DeleteUserRequest {
  }
  /**
   * 删除系统用户 res
   */
  interface _rdeleteUser extends _user.ResultBeanVoid {
  }
  /**
   * 修改密码 post
   */
  interface _peditPassword extends _user.EditPasswordRequest {
  }
  /**
   * 修改密码 res
   */
  interface _reditPassword extends _user.ResultBeanstring {
  }
  /**
   * 编辑系统用户 post
   */
  interface _peditUser extends _user.EditUserRequest {
  }
  /**
   * 编辑系统用户 res
   */
  interface _reditUser extends _user.ResultBeanVoid {
  }
  /**
   * 系统用户列表(总览) post
   */
  interface _plist extends _user.UsersRequest {
  }
  /**
   * 系统用户列表(总览) res
   */
  interface _rlist extends _user.ResultBeanPageDataListUserVO {
  }
  /**
   * 系统用户列表 post
   */
  interface _plistUsers extends _user.ListUsersRequest {
  }
  /**
   * 系统用户列表 res
   */
  interface _rlistUsers extends _user.ResultBeanPageDataListUserVO {
  }
  /**
   * 查询登录用户信息 post
   */
  interface _ploginUser {
  }
  /**
   * 查询登录用户信息 res
   */
  interface _rloginUser extends _user.ResultBeanSysUserVO {
  }
  /**
   * 新增系统用户 post
   */
  interface _psaveUser extends _user.SaveUserRequest {
  }
  /**
   * 新增系统用户 res
   */
  interface _rsaveUser extends _user.ResultBeanVoid {
  }
  /**
   * 系统用户详情 post
   */
  interface _puserDetail extends _user.UserDetailRequest {
  }
  /**
   * 系统用户详情 res
   */
  interface _ruserDetail extends _user.ResultBeanSysUserVO {
  }
}
declare module _default {
  interface apifuns {
    /**
     * 删除系统用户
     */
    _userdeleteUser({ data, taht }: { data: _user._pdeleteUser, taht?: any }): _user._rdeleteUser,
    /**
     * 修改密码
     */
    _usereditPassword({ data, taht }: { data: _user._peditPassword, taht?: any }): _user._reditPassword,
    /**
     * 编辑系统用户
     */
    _usereditUser({ data, taht }: { data: _user._peditUser, taht?: any }): _user._reditUser,
    /**
     * 系统用户列表(总览)
     */
    _userlist({ data, taht }: { data: _user._plist, taht?: any }): _user._rlist,
    /**
     * 系统用户列表
     */
    _userlistUsers({ data, taht }: { data: _user._plistUsers, taht?: any }): _user._rlistUsers,
    /**
     * 查询登录用户信息
     */
    _userloginUser({ data, taht }: { data: _user._ploginUser, taht?: any }): _user._rloginUser,
    /**
     * 新增系统用户
     */
    _usersaveUser({ data, taht }: { data: _user._psaveUser, taht?: any }): _user._rsaveUser,
    /**
     * 系统用户详情
     */
    _useruserDetail({ data, taht }: { data: _user._puserDetail, taht?: any }): _user._ruserDetail
  }
}
