export default {
  /**
   * post /user/deleteUser 删除系统用户
   */
  _userdeleteUser: { method: 'post', url: '/user/deleteUser' },
  /**
   * post /user/editPassword 修改密码
   */
  _usereditPassword: { method: 'post', url: '/user/editPassword' },
  /**
   * post /user/editUser 编辑系统用户
   */
  _usereditUser: { method: 'post', url: '/user/editUser' },
  /**
   * post /user/list 系统用户列表(总览)
   */
  _userlist: { method: 'post', url: '/user/list' },
  /**
   * post /user/listUsers 系统用户列表
   */
  _userlistUsers: { method: 'post', url: '/user/listUsers' },
  /**
   * get /user/loginUser 查询登录用户信息
   */
  _userloginUser: { method: 'get', url: '/user/loginUser' },
  /**
   * post /user/saveUser 新增系统用户
   */
  _usersaveUser: { method: 'post', url: '/user/saveUser' },
  /**
   * post /user/userDetail 系统用户详情
   */
  _useruserDetail: { method: 'post', url: '/user/userDetail' }
}
