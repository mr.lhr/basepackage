/**
 * 组织接口
 * create: lihuarong<517849815@qq.com>
 * time: 2020-06-10 16:50:58
 */
declare namespace _org {
  interface ListOrgRequest extends _default.post {
    /**
     * 页码
     */
    currentPage: number,
    /**
     * 页面大小
     */
    showCount: number
  }
  interface ResultBeanPageDataSysOrgVO extends _default.res {
    /**
     * 
     */
    data: _org.PageDataSysOrgVO
  }
  interface PageDataSysOrgVO {
    /**
     * 数据集合
     */
    rows: _org.SysOrgVO[],
    /**
     * 总行数
     */
    total: number
  }
  interface SysOrgVO {
    /**
     * 
     */
    createTime: string,
    /**
     * 
     */
    id: number,
    /**
     * 
     */
    name: string,
    /**
     * 
     */
    nameEn: string,
    /**
     * 
     */
    rights: string,
    /**
     * 
     */
    type: string,
    /**
     * 
     */
    updateTime: string
  }
  interface AddOrgRequest extends _default.post {
    /**
     * 组织名称
     */
    orgName: string,
    /**
     * 组织英文名
     */
    orgNameEn: string
  }
  interface ResultBeanstring extends _default.res {
    /**
     * 
     */
    data: string
  }
  interface ResultBeanListOrgVO extends _default.res {
    /**
     * 
     */
    data: _org.OrgVO[]
  }
  interface OrgVO {
    /**
     * 
     */
    id: number,
    /**
     * 
     */
    name: string
  }
  interface OrgAuthcEditRequest extends _default.post {
    /**
     * 菜单ID
     */
    menuIds: string,
    /**
     * 组织ID
     */
    orgId: string
  }
  interface OrgAuthcMenuRequest extends _default.post {
    /**
     * 组织ID
     */
    orgId: string
  }
  interface ResultBeanAuthcMenuVO extends _default.res {
    /**
     * 
     */
    data: _org.AuthcMenuVO
  }
  interface AuthcMenuVO {
    /**
     * 
     */
    idList: string[],
    /**
     * 
     */
    menuList: _org.Menu[]
  }
  interface Menu {
    /**
     * 
     */
    hasMenu: boolean,
    /**
     * 
     */
    isParent: boolean,
    /**
     * 
     */
    menuIcon: string,
    /**
     * 
     */
    menuId: number,
    /**
     * 
     */
    menuName: string,
    /**
     * 
     */
    menuOrder: string,
    /**
     * 
     */
    menuState: string,
    /**
     * 
     */
    menuType: string,
    /**
     * 
     */
    menuUrl: string,
    /**
     * 
     */
    parentId: string,
    /**
     * 
     */
    parentMenu: _org.Menu,
    /**
     * 
     */
    subMenu: _org.Menu[],
    /**
     * 
     */
    target: string
  }
  interface EditOrgRequest extends _default.post {
    /**
     * 组织ID
     */
    orgId: string,
    /**
     * 组织名称
     */
    orgName: string,
    /**
     * 组织英文名
     */
    orgNameEn: string
  }
  /**
   * 组织列表 post
   */
  interface _pListOrgs extends _org.ListOrgRequest {
  }
  /**
   * 组织列表 res
   */
  interface _rListOrgs extends _org.ResultBeanPageDataSysOrgVO {
  }
  /**
   * 新增组织 post
   */
  interface _paddOrg extends _org.AddOrgRequest {
  }
  /**
   * 新增组织 res
   */
  interface _raddOrg extends _org.ResultBeanstring {
  }
  /**
   * 组织列表 post
   */
  interface _pall {
  }
  /**
   * 组织列表 res
   */
  interface _rall extends _org.ResultBeanListOrgVO {
  }
  /**
   * 授权编辑 post
   */
  interface _pauthcEdit extends _org.OrgAuthcEditRequest {
  }
  /**
   * 授权编辑 res
   */
  interface _rauthcEdit extends _org.ResultBeanstring {
  }
  /**
   * 授权菜单 post
   */
  interface _pauthcMenu extends _org.OrgAuthcMenuRequest {
  }
  /**
   * 授权菜单 res
   */
  interface _rauthcMenu extends _org.ResultBeanAuthcMenuVO {
  }
  /**
   * 编辑组织 post
   */
  interface _peditOrg extends _org.EditOrgRequest {
  }
  /**
   * 编辑组织 res
   */
  interface _reditOrg extends _org.ResultBeanstring {
  }
}
declare module _default {
  interface apifuns {
    /**
     * 组织列表
     */
    _orgListOrgs({ data, taht }: { data: _org._pListOrgs, taht?: any }): _org._rListOrgs,
    /**
     * 新增组织
     */
    _orgaddOrg({ data, taht }: { data: _org._paddOrg, taht?: any }): _org._raddOrg,
    /**
     * 组织列表
     */
    _orgall({ data, taht }: { data: _org._pall, taht?: any }): _org._rall,
    /**
     * 授权编辑
     */
    _orgauthcEdit({ data, taht }: { data: _org._pauthcEdit, taht?: any }): _org._rauthcEdit,
    /**
     * 授权菜单
     */
    _orgauthcMenu({ data, taht }: { data: _org._pauthcMenu, taht?: any }): _org._rauthcMenu,
    /**
     * 编辑组织
     */
    _orgeditOrg({ data, taht }: { data: _org._peditOrg, taht?: any }): _org._reditOrg
  }
}
