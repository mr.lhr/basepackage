export default {
  /**
   * post /org/ListOrgs 组织列表
   */
  _orgListOrgs: { method: 'post', url: '/org/ListOrgs' },
  /**
   * post /org/addOrg 新增组织
   */
  _orgaddOrg: { method: 'post', url: '/org/addOrg' },
  /**
   * get /org/all 组织列表
   */
  _orgall: { method: 'get', url: '/org/all' },
  /**
   * post /org/authcEdit 授权编辑
   */
  _orgauthcEdit: { method: 'post', url: '/org/authcEdit' },
  /**
   * post /org/authcMenu 授权菜单
   */
  _orgauthcMenu: { method: 'post', url: '/org/authcMenu' },
  /**
   * post /org/editOrg 编辑组织
   */
  _orgeditOrg: { method: 'post', url: '/org/editOrg' }
}
