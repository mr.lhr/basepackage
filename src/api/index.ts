import Vue from 'vue'
import request from './request'
import urls from './RESTFULLURL'

const FUNS = {} as any
const urlObj: any = urls

Object.keys(urlObj).forEach((key) => {
  FUNS[key] = (options: any = {}) => {
    const obj: any = urlObj[key]
    options.method = obj.method || 'post'
    if (/{.*}/.test(obj.url) && options.data) {
      let arr = obj.url.split('/')
      arr = arr.map((item: string) => {
        if (!/{.*}/.test(item)) return item
        const temp = item.replace(/{|}/g, '')
        item = options.data[temp]
        delete options.data[temp]
        return item
      })
      obj.url = arr.join('/')
    }
    return request(obj.url, options)
  }
})

// 将API-model挂载到vue的原型上
// views引用的方法：this.$API 接口名（小驼峰）
Object.defineProperty(Vue.prototype, '$API', { value: FUNS })

const ENUMFUNS = {
  _gender: (data: string | number) => {
    let str = ''
    switch (data) {
      case 0:
      case '0':
        str = '未知'
        break
      case 1:
      case '1':
        str = '男'
        break
      case 2:
      case '2':
        str = '男'
        break
      default:
        str = '未知'
        break
    }
    return str
  },
  _prizeCouponstatus: (data: string) => {
    let str = ''
    switch (data) {
      case 'un_cash':
        str = '未兑换'
        break
      case 'await_send':
        str = '待发货'
        break
      case 'done':
        str = '已完成'
        break
      case 'expired':
        str = '已过期'
        break
      default:
        str = '未知'
        break
    }
    return str
  },
  _type: (data: string) => {
    let str = ''
    switch (data) {
      case 'material':
        str = '实物'
        break
      case 'virtual':
        str = '虚拟'
        break
      default:
        str = '位置'
        break
    }
    return str
  }
} as any

// 将通用转化挂载到vue的原型上
Object.defineProperty(Vue.prototype, '$ENUM', { value: ENUMFUNS })

export default FUNS
