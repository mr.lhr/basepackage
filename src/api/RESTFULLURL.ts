import credential from './model/credential'
import menu from './model/menu'
import org from './model/org'
import role from './model/role'
import user from './model/user'

export default {
  ...credential,
  ...menu,
  ...org,
  ...role,
  ...user,
  _userlogin: { method: 'post', url: '/login' }
}
