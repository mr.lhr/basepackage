import Qs from 'qs'
import axios, { AxiosRequestConfig } from 'axios'
import { base as baseURL } from '../env'
import { getToken, setToken, removeToken, getStatus, setStatus } from '@/utils/cookies'

const errMsg = {
  '-1': '远程服务响应失败,请稍后重试',
  400: '400: 错误请求',
  401: '401: 访问令牌无效或已过期',
  403: '403: 拒绝访问',
  404: '404：资源不存在',
  405: '405: 请求方法未允许',
  408: '408: 请求超时',
  500: '500：访问服务失败',
  501: '501：未实现',
  502: '502：无效网关',
  503: '503: 服务不可用'
} as any

function checkStatus(response?: any) {
  if (!response) {
    return { code: 'erroe', msg: '网络异常' }
  }

  // -1000 自己定义，连接错误的status
  console.info(response)
  const { status = -1000 } = response

  // 如果http状态码正常，则直接返回数据
  if ((status >= 200 && status < 300) || status === 304) {
    if (response.data.code === '1100') {
      setStatus(1)
      removeToken()
      location.href = '#/login'
      return false
    }
    // 如果不需要除了data之外的数据，可以直接 return response.data
    return response.data
  }
  return { code: status, msg: errMsg[status] || `连接错误${status}` }
}

// 添加一个请求拦截器 （于transformRequest之前处理）
axios.interceptors.request.use((config: any) => {
  // Do something before request is sent
  if (getToken()) {
    config.headers.authorization = getToken()
  }
  return config
}, (error) => {
  // 当出现请求错误是做一些处理
  return checkStatus(error)
})

// 添加一个返回拦截器 （于transformResponse之后处理）
// 返回的数据类型默认是json，若是其他类型（text）就会出现问题，因此用try,catch捕获异常
axios.interceptors.response.use((response) => {
  if (response && response.headers && response.headers.authorization) {
    setToken(response.headers.authorization)
  }
  return checkStatus(response)
}, (error) => {
  // 对返回的错误进行一些处理
  // 超时处理
  if (error.code === 'ECONNABORTED') {
    return
  }
  return checkStatus(error)
})

export default function request(url: string, {
  method = 'post',
  timeout = 250000,
  data = {},
  headers = {},
  dataType = 'json'
}: any) {
  if (getStatus() === '1' && url !== '/login') return false
  headers['Content-Type'] = 'application/json; charset=UTF-8'
  if (method === 'get') headers.Accept = 'application/json'
  if (url === '/login') headers['Content-Type'] = 'application/x-www-form-urlencoded'
  data.platformId = 1
  const defaultConfig = {
    baseURL,
    url,
    method,
    params: data,
    data,
    timeout,
    headers,
    responseType: dataType
  }

  if (method === 'get') {
    delete defaultConfig.data
  } else {
    delete defaultConfig.params

    const contentType = headers['Content-Type']

    if (typeof contentType !== 'undefined') {
      if (contentType.indexOf('multipart') !== -1) {
        // 类型 `multipart/form-data`
        defaultConfig.data = data
      } else if (contentType.indexOf('json') !== -1) {
        // 类型 `application/json`
        // 服务器收到的raw body(原始数据) "{name:"jhon",sex:"man"}"（普通字符串）
        defaultConfig.data = JSON.stringify(data)
      } else {
        // 类型 `application/x-www-form-urlencoded`
        // 服务器收到的raw body(原始数据) name=homeway&key=nokey
        defaultConfig.data = Qs.stringify(data)
      }
    }
  }
  return axios(defaultConfig as AxiosRequestConfig)
}
