import request from './request'
import urls from './RESTFULLURL'

let FUNS = {} as _default.apifuns
const urlObj: any = urls

Object.keys(urlObj).forEach((key) => {
  let temp = {} as any
  temp[key] = (options: any = {}) => {
    const obj: any = urlObj[key]
    options.method = obj.method || 'post'
    return request(obj.url, options)
  }
  FUNS = temp
})

export {
  FUNS
}