/**
 * API 通用返回值
 * create： lihuarong
 * time: 2020年5月6日 16点02分
 */

declare namespace _default {

  interface res {
    /**
     * 编码
     */
    code: string,

    /**
     * 错误提示
     */
    msg?: string,

    /**
     * 时间戳
     */
    timespan?: number
  }

  interface post {
    /** 
     * 请求系统编码 
     */
    reqCode?: string,

    /** 
     * 请求时间 
     */
    reqTime?: string,
  }

  interface listPagePost extends post {

    /** 
     * 页码
     */
    currentPage: number,

    /**
     *  页面大小
     */
    showCount: number
  }
  

  interface _plogin {
    username: string,
    password: string
  }

  interface _rlogin extends _default.res {
    data?: {
      name: string
    }
  }
  interface apifuns {
    /**
     * 用户登陆
     */
    _userlogin({ data, that }: { data: _default._plogin, that?: any }): _default._rlogin,
  }

  interface enumfuns {
    _gender(data: string | number): string,
    _prizeCouponstatus(data: string): string,
    _type(data: string): string,
  }
}
