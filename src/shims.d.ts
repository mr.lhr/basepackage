import Vue from 'vue'
declare interface Window {
  LOCAL_CONFIG?: any;
  tinymce?: any;
  webkitURL?: any;
}

declare module 'vue/types/vue' {
  interface Vue {
    $API: _default.apifuns;
    $ENUM: _default.enumfuns;
  }
}