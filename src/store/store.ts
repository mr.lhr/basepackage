
import store from 'store'
import { storeKey } from '@/env'

export interface storeTemp {
  key: string
  value?: string | object
}

class Store {
  public prefix: string

  constructor() {
    this.prefix = storeKey
  }
	
  async set({ key, value = "" }: storeTemp) {
    store.set(`${this.prefix}${key}`, typeof value == "object" ? JSON.stringify(value) : value)
    return true
  }

  get({ key }: storeTemp) {
    if (!key) { throw new Error('没有找到key。') }
    if (typeof key === 'object') { throw new Error('key不能是一个对象。') }
    let value = store.get(`${this.prefix}${key}`)
    if (value && value !== null) { value = JSON.parse(value) }
    return value
  }

  remove({ key }: storeTemp) {
    store.remove(`${this.prefix}${key}`)
  }

}

const storeV = new Store()
export default storeV
