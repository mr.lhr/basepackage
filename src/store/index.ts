import Vue from 'vue'
import Vuex, { Store } from 'vuex'
import { IApp } from './modules/app'
import { IErrorLog } from './modules/errorLog'
import { IPermission } from './modules/permission'
import { ISettings } from './modules/settings'
import { ITagsView } from './modules/tagsView'
import { IUser } from './modules/user'

Vue.use(Vuex)

export interface IRootState {
  app: IApp
  errorLog: IErrorLog
  permission: IPermission
  settings: ISettings
  tagsView: ITagsView
  user: IUser
}

// Declare empty store first, dynamically register all modules later.
const store: Store<IRootState> = new Vuex.Store<IRootState>({})
export default store
