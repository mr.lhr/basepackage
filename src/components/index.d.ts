/**
 * 主键类型
 * create：lihuarong
 * 时间: 2020年5月13日 2020年5月13日
 */

declare namespace _comUtable {

  interface button {
    size?: string,
    type?: string,
    click?: Function,
    lclick?: Function,
    label?: string,
    lableChang?: Function | object
  }

  interface props {
    prop?: string,
    label?: string,
    button?: button[],
    a?: button[],
    class?: string,
    change?: Function,
    type?: string,
    width?: string | number,
    
    /**
     * 头部提醒
     */
    renderHeader?: string,
    
    /**
     * 图片 + 文字
     * prop
     */
    image?: string,
    size?: number
  }

  /**
   * 通用表格
   */
  interface table {
    isTrue?: boolean,
    props: props[],
    tableData?: any[],
    tableDataU?: any,
    dbClickRow?: Function
  }

  /**
   * 通用尾部
   */
  interface pagination {
    isTrue?: boolean,
    total?: number,
    showCount?: number,
    currentPage?: number,
    click?: Function
  }

  /**
   * Array[
   *    label: string | number,
   *    value: string | number | boolean 
   * ]
   */
  interface optionM {
    label: string | number,
    value: string | number | boolean 
  }

  interface selectProps {
    type: 'input' | 'btn' | 'select' | 'reBtn' | 'enbtn' | 'year-month' | 'year-month-day' | 'year-month-day-two' | 'cascader',
    prop?: string,
    /**
     * type === 'select'
     */
    data?: any[] | optionM[],
    click?: Function,
    vformat?: string,
    pickeseparator?: string,
    placeholder?: string,
    splaceholder?: string,
    eplaceholder?: string,
    style?: string,
    class?: '' | 'primary' | 'success' | 'info' | 'warning' | 'danger',
    label?: string,
    prop1?: string,
    prop2?: string,
    all?: string,
    options?: any,
    props?: object,
  }

  /**
   * 搜索框
   */
  interface select {
    isTrue?: boolean,
    props: selectProps[],
    filters: any,
    filtersOld?: any
  }

  /**
   * 表格上方tabs
   */
  interface tabs {
    data: string[],
    index: number,
    click: Function
  }
}