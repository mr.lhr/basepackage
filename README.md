# basePackage

# basePackage
## 项目结构
```

```
## 项目安装部署
```
yarn install
yarn run serve
```
## shell.js 脚本命令
### 1. 更新版本(-u)
```
// 不带参数 可直接获取基本版本的更新
node ./shell.js -u

// 带参数 -l  指定更新的git版本 可带账号密码
1. node ./shell.js -l gitlab.com/mr.lhr/applet-basepackage.git -u
2. node ./shell.js -l http://gitlab.com/mr.lhr/applet-basepackage.git -u
3. node ./shell.js -l http://username:password@gitlab.com/mr.lhr/applet-basepackage.git

// 带参数 -B 指定git版本的分支
1. node ./shell.js -B dev_1.0 -l gitlab.com/mr.lhr/applet-basepackage.git -u
2. node ./shell.js -B dev_1.0 -l http://gitlab.com/mr.lhr/applet-basepackage.git -u
3. node ./shell.js -B dev_1.0 -l http://username:password@gitlab.com/mr.lhr/applet-basepackage.git
```

#### 注： 指令-u 需要在最后面

### 2. 创建vue页面(-f)
```
// 路径格式为 两级目录 
// 例如: -f a 会在目录的views下创建 a.vue页面
//       -f a/b 会在目录的views下创建 a目录 b.vue页面
// 同时会在 router business 下 追加路由
 
// 不带参数
node ./shell.js -f a/a

// -n 页面路由的title 
node ./shell.js -n title -f a/a

// -m 路由所属模块
node ./shell.js -m module -n title -f a/a

```
#### 注： 指令-f 需要在最后面

### 3. 删除vue页面(-d)
```
// 路径格式为 两级目录 
// 例如: -d a 会删除目录的pages下 a.vue页面
//       -d a/b 会删除目录的pages下 a目录 下 b.vue页面
// 当 a目录下没有页面是，默认删除目录
 
// 例子 删除
1. node ./shell.js -d a/a

// 删除 但不删除目录
2. node ./shell.js -i 0 -d a/a

// 如果配置了模块 需要带上-m
3. node ./shell.js -m module -i 0 -d a/a

// 通用表格模板 -t 1
3. node ./shell.js -t 1 -m module -i 0 -d a/a
```
#### 注： 指令-d 需要在最后面

### 4. 根据swagger生成api 的d.ts(-s)
```
// -w swagger 的文档地址
1. node ./shell.js -w http://192.168.3.220:8201/v2/api-docs -s
```
#### 注： 指令-s 需要在最后面

### 5. 生成src/api/model index.d.ts(-r)
```
// -r
1. node ./shell.js -r
```

### 6. 生成vuex文件(-x)
```
// -x 文件名称
1. node ./shell.js -x filename
2. node ./shell.js -n title -x filename
```
#### 注： 指令-x 需要在最后面

### 7. 删除vuex文件(-X)
```
// -X 文件名称
1. node ./shell.js -X filename
```
