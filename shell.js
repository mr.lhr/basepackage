/**
 * shell 脚本
 * by: mr.lhr
 * time: 2020年6月8日 13点23分
 */
const config = require('./shellconfig.json')
let SWAGGERURL = config && config.SWAGGERURL ? config.SWAGGERURL : 'http://192.168.3.220:8202/v2/api-docs'
const SWAGGERSPACE = ''
const GITOBJ = config && config.GITOBJ ? config.GITOBJ : {
  branch: 0,
  path: 'gitlab.com/mr.lhr/basepackage.git'
}

const request = require('request')
const program = require('commander')
const ora = require('ora')
const chalk = require('chalk')
const shell = require('shelljs')
const spinner = ora('running')
const fs = require('fs')
const path = require('path')

const FILEOBJ = {
  /**
   * pages name
   */
  name: '',
  /**
   * yes or no to del dir
   */
  isDelDir: 1,
  /**
   * pulldown
   */
  pullDown: 0,
  /**
   * ReachBottom
   */
  reachBottom: 0,
  /**
   * routerDir
   */
  routerDir: 'business',
  /**
   * utable
   */
  utable: 0,
  /**
   * router path
   */
  extendsPath: ''
}

/**
 * check file or dir does it exist
 */
const checkFDExist = (name, path) => fs.readdirSync(path).includes(name)

/**
 * sleep
 */
const sleep = (time) => new Promise(resolve => setTimeout(() => resolve(), time))

/**
 * api
 */
const RESTAPI = () => {
  console.log(chalk.gray('RESTAPI'))
  spinner.start('RESTAPI handleing...')
  const filePath = path.resolve('./src/api/model')
  const pathArr = fs.readdirSync(filePath)
  const rw = fs.createWriteStream('./src/api/RESTFULLURL.ts')
  const rwModel = fs.createWriteStream('./src/api/model/index.d.ts')
  const importArr = []
  const exportArr = []
  const modelArr = []
  pathArr.map(item => {
    if (item.includes('.')) return false
    importArr.push(`import ${item} from './model/${item}'`)
    exportArr.push(`  ...${item}`)
    modelArr.push(`///<reference path="./${item}/index.d.ts" />`)
  })
  if (importArr) {
    exportArr.push("  _userlogin: { method: 'post', url: '/login' }")
    const rwStr = `${importArr.join('\n')}\n\nexport default {\n${exportArr.join(',\n')}\n}\n`
    rw.write(rwStr)
    rwModel.write(`${modelArr.join('\n')}\n`)
  }
  spinner.succeed('RESTAPI done')
  return true
}

/**
 * router
 */
const ROUTER = () => {
  console.log(chalk.gray('ROUTER'))
  spinner.start('ROUTER handleing...')
  const filePath = path.resolve('./src/router')
  const pathArr = fs.readdirSync(filePath)
  const rw = fs.createWriteStream('./src/router/appendRouter.ts')
  const importArr = []
  const exportArr = []
  pathArr.map(item => {
    if (item.includes('.')) return false
    importArr.push(`import ${item} from './${item}'`)
    exportArr.push(`  ...${item}`)
  })
  if (importArr) {
    const rwStr = `${importArr.join('\n')}${importArr.length > 0 ? '\n' : ''}export default [\n${exportArr.join(',\n')}\n]\n`
    rw.write(rwStr)
  }
  spinner.succeed('ROUTER done')
  return true
}

/**
 * upgrade
 */
const UPGRADE = async() => {
  console.log(chalk.gray('template start download'))
  spinner.start('downloading')
  shell.rm('-rf', 'basePackage')
  const gitPath = `${GITOBJ.branch === 0 ? '' : `-b ${GITOBJ.branch}`} ${GITOBJ.path.includes('http://') || GITOBJ.path.includes('https://') ? `${GITOBJ.path}` : `http://${GITOBJ.path}`}`
  const { code, stdout, stderr } = await shell.exec(`git clone ${gitPath} basePackage`)
  if (code !== 0) {
    console.log(chalk.red('components install fail'))
    console.log(chalk.red('Exit code:', code))
    console.log(chalk.red('output:', stdout))
    console.log(chalk.red('stderr:', stderr))
    return spinner.fail('fail')
  }
  const bPackage = require('./basePackage/package.json')
  const nPackage = require('./package.json')
  let isInstall = false
  if (bPackage && nPackage) {
    for (const key in bPackage.dependencies) {
      if (!nPackage.dependencies[key]) {
        nPackage.dependencies[key] = bPackage.dependencies[key]
        isInstall = true
      }
    }
    for (const key in bPackage.devDependencies) {
      if (!nPackage.devDependencies[key]) {
        nPackage.devDependencies[key] = bPackage.devDependencies[key]
        isInstall = true
      }
    }
    if (bPackage.upgrade && bPackage.upgrade.path && bPackage.upgrade.path.length > 0) {
      for (let i = 0; i < bPackage.upgrade.path.length; i++) {
        const item = bPackage.upgrade.path[i]
        await shell.rm('-rf', item)
        // if (!item.includes('.') && item.includes('/')) await shell.mkdir(item)
        await shell.cp('-rf', `basePackage/${item}`, item)
        if (item.includes('/api/model')) await RESTAPI()
        if (item.includes('router/')) await ROUTER()
      }
    }
    nPackage.upgrades ? nPackage.upgrades[bPackage.name] = bPackage.version : nPackage.upgrades = { [bPackage.name]: bPackage.version }
    const rw = fs.createWriteStream('./package.json')
    await rw.write(JSON.stringify(nPackage, null, '\t') + '\n', async() => {
      if (isInstall) await shell.exec('yarn install')
    })
  }
  await shell.rm('-rf', 'basePackage')
  spinner.succeed('complete')
}

const requestAsync = function(url) {
  return new Promise(function(resolve, reject) {
    request.get(url, (err, response, body) => {
      if (err) return reject(err)
      resolve(body && typeof body !== 'object' ? JSON.parse(body) : body)
    })
  })
}

/**
 * 获取日期
 */
const addZore = (str) => {
  str = `${str}`
  return str.length > 1 ? str : `0${str}`
}
const getTimeStr = () => {
  const now = new Date()
  return `${now.getFullYear()}-${addZore(now.getMonth() + 1)}-${addZore(now.getDate())} ${addZore(now.getHours())}:${addZore(now.getMinutes())}:${addZore(now.getSeconds())}`
}

/**
 * swagger
 */
const handlePaths = (APIKey) => {
  const apiKeyArr = APIKey.split('/')
  const len = apiKeyArr.length
  let _apiKey = len > 0 ? `_${apiKeyArr[0]}` : ''
  let faceName = apiKeyArr[len - 1]
  const params = []
  if (len >= 2) {
    let index = 1
    _apiKey = `_${apiKeyArr[len - 2]}${apiKeyArr[len - 1]}`
    while (/{.*}/.test(faceName)) {
      params.push(faceName.replace(/{|}/g, ''))
      index += 1
      faceName = apiKeyArr[len - index]
      _apiKey = `_${apiKeyArr[len - index - 1]}${apiKeyArr[apiKeyArr.length - index]}`
    }
  }
  return { apiKeyArr, _apiKey, faceName, params }
}
const SWAGGER = async() => {
  console.log(chalk.gray('get swagger start'))
  spinner.start('getting\n')
  const res = await requestAsync(SWAGGERURL)
  if (!res) return spinner.fail('get swagger error')
  const { tags, paths, definitions } = res
  const fileModel = {}
  tags.map(item => {
    fileModel[item.name] = {
      path: item.description.replace(/Controller|\s*/g, '').replace(/[\u4e00-\u9fa5]/g, '').toLowerCase()
    }
  })

  const returnEts = (ref) => {
    if (/Query.*CliDTO/.test(ref)) return { ets: 'extends _default.listPagePost ', del: ['currentPage', 'reqCode', 'reqTime', 'showCount'] }
    if (/Update.*CliDTO/.test(ref)) return { ets: 'extends _default.post ', del: ['reqCode', 'reqTime'] }
    if (/ResultBean.*/.test(ref)) return { ets: 'extends _default.res ', del: ['code', 'msg', 'timespan'] }
    if (/Request/.test(ref)) return { ets: 'extends _default.post ', del: ['reqCode', 'reqTime'] }
    return { ets: '', del: [] }
  }
  const changeKeytype = {
    string: 'string',
    integer: 'number',
    boolean: 'boolean'
  }
  const changeKey = async(refs, space, medel, params, faceName) => {
    if (params && params.length > 0) {
      const key = `${faceName}Params`
      medel[key] = [`  interface ${key} {`]
      params.map(item => {
        medel[key].push(`    ${item}: string,`)
      })
      medel[key].push('  }')
      return `extends ${space}.${key} `
    }
    // a
    if (!refs) return ''
    const scheamArr = refs.split('/')
    const key = scheamArr[scheamArr.length - 1]
    const mkey = key.replace(/«|»/g, '')
    const obj = definitions[key]
    if (!obj.properties) return {}
    if (!medel[key]) {
      const { ets, del } = returnEts(mkey)
      medel[key] = [`  interface ${mkey} ${ets}{`]
      for (const skey in obj.properties) {
        if (!del.includes(skey)) {
          const interItem = obj.properties[skey]
          let itemEts = ''
          if ((interItem.items && interItem.items.$ref) || (interItem && interItem.$ref)) {
            const $ref = interItem.$ref ? interItem.$ref : interItem.items.$ref
            const sscheamArr = $ref.split('/')
            const sskey = sscheamArr[sscheamArr.length - 1]
            const smkey = sskey.replace(/«|»/g, '')
            itemEts = `${space}.${smkey}`
            await changeKey($ref, space, medel)
          }
          const arr = ['    /**', `     * ${interItem.description || ''}`, '     */']
          medel[key].push(...arr)
          let val = itemEts
          if (interItem.type === 'array' && itemEts) val = `${itemEts}[]`
          if (interItem.type === 'array' && !itemEts) val = `${changeKeytype[interItem.items.type]}[]`
          if (interItem.type !== 'array' && !itemEts) val = changeKeytype[interItem.type]
          medel[key].push(`    ${skey}: ${val},`)
        }
      }
      if (medel[key].length > 1) {
        medel[key][medel[key].length - 1] = medel[key][medel[key].length - 1].substr(0, medel[key][medel[key].length - 1].length - 1)
      }
      medel[key].push('  }')
    }
    const ets = `extends ${space}.${mkey} `
    return ets
  }
  for (const key in paths) {
    const item = paths[key]
    const sitem = item.post || item.get
    const parameters = sitem.parameters && sitem.parameters[0].schema && sitem.parameters[0].schema.$ref ? sitem.parameters[0].schema.$ref : ''
    const responses = sitem.responses && sitem.responses[200] && sitem.responses[200].schema && sitem.responses[200].schema.$ref ? sitem.responses[200].schema.$ref : ''

    const joinObj = {
      method: item.post ? 'post' : 'get',
      name: sitem.summary,
      parameters,
      responses
    }
    fileModel[sitem.tags].api ? fileModel[sitem.tags].api[key] = joinObj : fileModel[sitem.tags].api = { [key]: joinObj }
  }
  await shell.rm('-rf', 'src/api/model/*')
  for (const key in fileModel) {
    const item = fileModel[key]
    const path = `src/api/model/${item.path}`
    console.log(chalk.gray(`-handleing ${key} path: ${path}`))
    await shell.mkdir(path)
    const indexD = `${path}/index.d.ts`
    const index = `${path}/index.ts`
    await shell.touch(indexD)
    await shell.touch(index)
    const indexDrw = fs.createWriteStream(indexD)
    const indexrw = fs.createWriteStream(index)
    let indexrwDStr = []
    const indexrwDAStr = ['declare module _default {', '  interface apifuns {']
    const indexrwStr = ['export default {']
    const model = {}
    for (const APIKey in item.api) {
      console.log(chalk.gray(`                api: ${APIKey}`))
      const { _apiKey, faceName, params } = handlePaths(APIKey)
      const apiItem = item.api[APIKey]
      const aTitle = `   * ${apiItem.method} ${APIKey} ${apiItem.name}`
      const title = ['  /**', aTitle, '   */']
      // d.ts
      title[1] = `   * ${apiItem.name} ${item.api[APIKey].method}`
      indexrwDStr.push(...title)
      let ext = await changeKey(apiItem.parameters, `_${item.path}`, model, params, faceName)
      indexrwDStr.push(`  interface _p${faceName} ${ext}{`)
      indexrwDStr.push('  }')
      title[1] = `   * ${apiItem.name} res`
      indexrwDStr.push(...title)
      ext = await changeKey(apiItem.responses, `_${item.path}`, model)
      indexrwDStr.push(`  interface _r${faceName} ${ext}{`)
      indexrwDStr.push('  }')
      // api
      const api = ['    /**', `     * ${apiItem.name}`, '     */']
      indexrwDAStr.push(...api)
      indexrwDAStr.push(`    ${_apiKey}({ data, taht }: { data: _${item.path}._p${faceName}, taht?: any }): _${item.path}._r${faceName},`)
      // url
      title[1] = aTitle
      indexrwStr.push(...title)
      indexrwStr.push(`  ${_apiKey}: { method: '${apiItem.method}', url: '${APIKey}' },`)
    }
    indexrwStr[indexrwStr.length - 1] = indexrwStr[indexrwStr.length - 1].replace('},', '}')
    indexrwStr.push('}')
    indexrw.write(`${indexrwStr.join('\n')}\n`)
    indexrwDStr.push('}')
    if (indexrwDAStr.length > 1) {
      indexrwDAStr[indexrwDAStr.length - 1] = indexrwDAStr[indexrwDAStr.length - 1].substr(0, indexrwDAStr[indexrwDAStr.length - 1].length - 1)
    }
    indexrwDAStr.push('  }')
    indexrwDAStr.push('}')
    indexrwDStr.push(...indexrwDAStr)
    const modelArr = []
    for (const modelKey in model) {
      modelArr.push(...model[modelKey])
    }
    indexrwDStr = [...['/**', ` * ${key}`, ' * create: lihuarong<517849815@qq.com>', ` * time: ${getTimeStr()}`, ' */', `declare namespace _${item.path} {`], ...modelArr, ...indexrwDStr]
    indexDrw.write(`${indexrwDStr.join('\n')}\n`)
  }
  await RESTAPI()
  spinner.succeed('SWAGGER done')
  // console.info(JSON.stringify(fileModel, null, '\t'))
}

// file
const fToC = ([first, ...rest]) => `${first.toUpperCase()}${rest.join('')}`

const handlePages = async(pathArr, isDel) => {
  const routerPath = 'src/router'
  const fileE = checkFDExist(FILEOBJ.routerDir, routerPath)
  const indexPath = `${routerPath}/${FILEOBJ.routerDir}/index.ts`
  const jsonPath = `${routerPath}/${FILEOBJ.routerDir}/index.json`
  const path = pathArr[pathArr.length - 1]
  const component = `@/views/${pathArr.length === 1 ? pathArr[0] : `${pathArr[pathArr.length - 2]}/${pathArr[pathArr.length - 1]}`}.vue`
  if (!fileE) {
    await shell.mkdir(`${routerPath}/${FILEOBJ.routerDir}`)
    await shell.touch(indexPath)
    await shell.touch(jsonPath)
    const jsrw = fs.createWriteStream(jsonPath)
    jsrw.write(`${JSON.stringify({}, null, '\t')}\n`)
    ROUTER()
    await sleep(800)
  }
  const json = require(`./${jsonPath}`)
  isDel ? delete json[path] : json[path] = {
    path: FILEOBJ.extendsPath ? `${path}/${FILEOBJ.extendsPath}` : path,
    component,
    pathName: path,
    name: FILEOBJ.name || path
  }
  const childArr = []
  for (const i in json) {
    const item = json[i]
    childArr.push([
      '    /**\n',
      `      * ${item.name}\n`,
      '      */\n',
      '    {\n',
      `      path: '/${item.path}',\n`,
      `      component: () => import(/* webpackChunkName: "${item.component.replace('@/views/', '').replace('.vue', '').replace('/', '-')}" */ '${item.component}'),\n`,
      `      name: '${item.pathName}',\n`,
      `      meta: { title: '${item.name}' }\n`,
      '    }'
    ].join(''))
  }
  const jsrw = fs.createWriteStream(jsonPath)
  jsrw.write(`${JSON.stringify(json, null, '\t')}\n`)
  const rw = await fs.createWriteStream(indexPath)
  rw.write(`/* Layout */\nimport Layout from '@/layout/index.vue'\n\nexport default [{\n  path: '/${FILEOBJ.routerDir}',\n  component: Layout,\n  meta: {\n    alwaysShow: true\n  },\n  children: [\n${childArr.join(',\n')}\n  ]\n}]\n`)
  return true
}
const getPath = (path) => {
  const pathArr = `${path}`.split('/')
  const filePath = pathArr.length === 1 ? `${pathArr[0]}.vue` : `${pathArr[pathArr.length - 2]}/${pathArr[pathArr.length - 1]}.vue`
  return { filePath, pathArr }
}
const utable = [
`    <Utable
      ref="Utable"
      :table="table"
      :pagination="pagination"
      :select="select"
    />`,
"import Utable from '@/components/Utable/index.vue'",
`@Component({
  components: {
    Utable
  }
})`,
`  // refs 结构
  $refs!: {
    Utable: Utable
  }`,
`  private select: _comUtable.select = {
    props: [
      { type: 'input', prop: 'module', placeholder: '模板' },
      { type: 'enbtn', click: this.getList, label: '查询', class: 'primary' },
      { type: 'reBtn' }
    ],
    filters: {
      showCount: 10,
      currentPage: 1
    } // as _modlue._plist
  }`,
`  private table: _comUtable.table = {
    props: [
      { prop: 'module', label: '模板' },
      {
        label: '操作',
        a: [
          { click: this.getList, label: '查看', type: 'primary' }
        ]
      }
    ],
    tableDataU: {} // as _member._rlist
  }`,
`  private pagination: _comUtable.pagination = {
    click: this.getList,
    showCount: 10,
    currentPage: 1
  }`,
`  async getList() {
    this.$refs.Utable.loading = true
    // this.table.tableDataU = await this.$API._memberlist({ data: this.select.filters })
    this.$refs.Utable.loading = false
  }`
]
const FILE = async(path) => {
  if (!path) return console.log(chalk.gray('-path is no null'))
  const { filePath, pathArr } = getPath(path)
  let fileAllPath = 'src/views/'
  // const isW = checkFDExist()
  const temp =
`<template>
  <div class="dashboard-container"${FILEOBJ.utable ? `>\n${utable[0]}\n  </div>` : ' />'}
</template>

<script lang="ts">
import { Component, Vue } from 'vue-property-decorator'${FILEOBJ.utable ? `\n${utable[1]}` : ''}

${FILEOBJ.utable ? utable[2] : '@Component({})'}
export default class ${fToC(pathArr[pathArr.length - 1])} extends Vue {${FILEOBJ.utable ? `\n${utable[3]}\n\n${utable[4]}\n\n${utable[5]}\n\n${utable[6]}\n\n${utable[7]}\n` : ''}
  async mounted() {
    // mounted
  }

  async created() {
    // created
  }
}
</script>

<style lang="scss" scoped>

</style>
`

  if (pathArr.length > 1) await shell.mkdir(`${fileAllPath}${pathArr[pathArr.length - 2]}`)
  fileAllPath = `${fileAllPath}${filePath}`
  const { code, stdout, stderr } = await shell.touch(fileAllPath)
  if (code !== 0) {
    console.log(chalk.red('FILE create fail'))
    console.log(chalk.red('Exit code:', code))
    console.log(chalk.red('output:', stdout))
    console.log(chalk.red('stderr:', stderr))
    return spinner.fail('fail')
  }
  const rw = fs.createWriteStream(fileAllPath)
  rw.write(temp)
  await handlePages(pathArr)
  console.log(chalk.gray(`-handleing path: ${fileAllPath}`))
  spinner.succeed('FILE done')
}

const DFILE = async(path) => {
  const { filePath, pathArr } = getPath(path)
  await shell.rm('-rf', `src/views/${filePath}`)
  await handlePages(pathArr, true)
  let num = 0
  if (pathArr.length > 1) {
    const dir = `src/views/${pathArr[pathArr.length - 2]}`
    await shell.ls(`${dir}/*.vue`).forEach(() => {
      num += 1
    })
    if (num === 0 && FILEOBJ.isDelDir) await shell.rm('-rf', dir)
  }
  spinner.succeed('DFILE done')
}

// vuex
const iNITVUX = () => {
  const vuexIndexImport = []
  const key = []
  let keyVal = ''
  const vuexIndexfirst = ['import Vue from \'vue\'', 'import Vuex, { Store } from \'vuex\'']
  const vuexIndexContent = ['\nexport interface IRootState {']
  const vuexIndexEnd = [
    '}\n',
    '// Declare empty store first, dynamically register all modules later.',
    'const store: Store<IRootState> = new Vuex.Store<IRootState>({})',
    'export default store\n'
  ]
  const filePath = path.resolve('./src/store/modules')
  const pathArr = fs.readdirSync(filePath)
  pathArr.map(item => {
    if (!item.includes('.ts')) return false
    const name = item.split('.')[0]
    const tname = fToC(name)
    key.push(`  ${name}: I${tname}`)
    vuexIndexImport.push(`import { I${tname} } from './modules/${name}'`)
  })
  keyVal = key.join('\n')
  const rw = fs.createWriteStream('src/store/index.ts')
  rw.write([...vuexIndexfirst, ...vuexIndexImport, '\nVue.use(Vuex)', ...vuexIndexContent, [keyVal], ...vuexIndexEnd].join('\n'))
  spinner.succeed('iNITVUX done')
}

const VUEX = async(name) => {
  const fileName = fToC(name)
  const modulesFile = ['/**', ` * ${FILEOBJ.name || name}`, ' * by: lihuarong', ` * time: ${getTimeStr()}`, ' */',
    'import { VuexModule, Module as vueMoudle, Action, Mutation, getModule } from \'vuex-module-decorators\'',
    'import store from \'@/store\'',
    'import localStore from \'@/store/store\'',
    `const key = '${name}' as string`,
    `export interface I${fileName} {`,
    '}\n',
    '@vueMoudle({ dynamic: true, store, name: key })',
    `class ${fileName} extends VuexModule implements I${fileName} {`,
    '  // interface temp\n',
    '  /**',
    '    * @Mutation',
    '    * private mution() {',
    '    * }',
    '    */\n',
    '  /**',
    '    * @Action',
    '    * public action() {',
    '    * }',
    '    */',
    '}\n',
    `export const ${fileName}Module = getModule(${fileName})\n`
  ]
  const rw = fs.createWriteStream(`src/store/modules/${name}.ts`)
  rw.write(modulesFile.join('\n'))
  iNITVUX()
  spinner.succeed('VUEX done')
}

const VUEXD = async(name) => {
  await shell.rm('-rf', `src/store/modules/${name}.ts`)
  iNITVUX()
  spinner.succeed('VUEX done')
}

program.version('0.0.1', '-v, -version')
  .option('-e, --router', 'handle router')
  .option('-r, --restapi', 'handle restapi')
  .option('-u, --upgrade [arg...]', 'upgrade template param:(branch: 0 or name, git)')
  .option('-w, --swaggerDoc <url>', 'swaggerDoc url')
  .option('-s, --swagger [arg...]', 'handel swagger')
  .option('-B, --branch <branch>', 'git branch')
  .option('-l, --giturl <url>', 'giturl')
  .option('-n, --pagesName <name>', 'pages name')
  .option('-x, --vuex <name>', 'create vuex file Temp')
  .option('-X, --vuexD <name>', 'del vuex file Temp')
  .option('-i, --delDir [is]', 'del dir')
  .option('-d, --filed <path>', 'handel del file')
  .option('-f, --file <path>', 'handel add file')
  .option('-m, --routerModul <name>', 'handel add file')
  .option('-t, --utable <is>', 'handel add file utable')
  .option('-p, --extendsPath <path>', 'handel router extendsPath')

program.on('option:router', () => ROUTER())

program.on('option:restapi', () => RESTAPI())

program.on('option:upgrade', () => UPGRADE())

program.on('option:swagger', () => SWAGGER())

program.on('option:pagesName', (name) => { FILEOBJ.name = name })

program.on('option:vuex', (name) => VUEX(name))

program.on('option:vuexD', (name) => VUEXD(name))

program.on('option:delDir', (is) => { FILEOBJ.isDelDir = /\d/.test(is) ? parseInt(is) : 1 })

program.on('option:utable', (is) => { FILEOBJ.utable = /\d/.test(is) ? parseInt(is) : 1 })

program.on('option:branch', (branch) => { GITOBJ.branch = branch })

program.on('option:file', (path) => FILE(path))

program.on('option:filed', (path) => DFILE(path))

program.on('option:routerModul', (name) => { FILEOBJ.routerDir = name })

program.on('option:giturl', (url) => { GITOBJ.path = url })

program.on('option:extendsPath', (path) => { FILEOBJ.extendsPath = path })

program.on('option:swaggerDoc', (url) => { SWAGGERURL = url })

program.parse(process.argv)
